import { showModal } from './modal';
import { createElement } from '../../helpers/domHelper';
import  App  from './../../app';

export function showWinnerModal(fighter) {
  const bodyElement = createModalBody(fighter);

  return showModal({ title: 'Congratulations', onClose: onModalClose, bodyElement });
}

function onModalClose(){
  const arenaElement = document.querySelector('.arena___root');
  arenaElement.remove();

  return new App();
}

function createModalBody(fighter){
  const hintElement = createElement({ tagName: 'span', className: 'modal__hint' });
  hintElement.innerText = `Fighter ${fighter.name} wins`;

  const bodyElement = createElement({ tagName: 'div', className: 'modal__body' });
  bodyElement.append(hintElement);

  return bodyElement;
}
