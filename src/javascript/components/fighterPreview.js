import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const imageElement = createFighterImage(fighter);
    const fighterInformation = createFighterDetails(fighter);

    fighterElement.append(imageElement, fighterInformation);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterDetails({ source, _id, ...fighter }){
  const details = Object.keys(fighter).map(characteristic => {
    const detailElement = createElement({ tagName: "div", className: "fighter-detail" });
    detailElement.innerText = characteristic + ": " + fighter[characteristic];
    return detailElement;
  });

  const detailsList = createElement({ tagName: "div", className: "fighter-details" });
  detailsList.append(...details);
  return detailsList;
}
