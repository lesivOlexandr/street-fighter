import { controls } from '../../constants/controls';

const SECOND = 1000;

export async function fight(firstFighter, secondFighter) {
  const firstHealthIndicator = document.getElementById('left-fighter-indicator');
  const secondHealthIndicator = document.getElementById('right-fighter-indicator');

  // Копируем свойства объекта, чтоб исходные не изменились
  firstFighter = { ...firstFighter };
  secondFighter = { ...secondFighter };

  // Между использованиями специальной способности должен быть перерыв в 10 секунд
  // в этих переменных мы будет сохранять время последнего применения
  let lastTimeOfHitFirstPlayer = 0;
  let lastTimeOfHitSecondPlayer = 0;

  const fighterOneStarterDefense = firstFighter.defense;
  const fighterTwoStarterDefense = secondFighter.defense;

  const fighterOneStarterHealth = firstFighter.health;
  const fighterTwoStarterHealth = secondFighter.health;

  // Множества, в которые добавляем нажатые клавишы для специальных приемов
  const firstPlayerSpecialKeysPressed = new Set();
  const secondPlayerSpecialKeysPressed = new Set();

  return new Promise((resolve) => {

    document.addEventListener('keydown', handleKeydown);
    document.addEventListener('keyup', handleKeyup);

    function handleKeydown(event){
      switch (event.code) {
        case controls.PlayerOneAttack:
          if(firstFighter.defense != 100){
            secondFighter.health -= getDamage(firstFighter, secondFighter);
          }
          break;
  
        case controls.PlayerTwoAttack:
          if(secondFighter.defense != 100){
            firstFighter.health -= getDamage(secondFighter, firstFighter);
          }
          break;

        // Если игрок держит клавишу блока, то устанавливаем значение защиты 100, чтоб урон не наносился
        case controls.PlayerOneBlock:
          firstFighter.defense = 100; 
          break;
        
        case controls.PlayerTwoBlock:
          secondFighter.defense = 100;
          break;

        default:
          // Если игрок нажал не на клавишу блока или атаки, тогда обрабатываем случай проверки на клавишы критического удара
          if (lastTimeOfHitFirstPlayer + SECOND * 10 < Date.now()) {
            if (controls.PlayerOneCriticalHitCombination.includes(event.code)) {
              firstPlayerSpecialKeysPressed.add(event.code);
            }
            // Если нажаты 3 из 3 клавиш необходимых для критического удара
            if (firstPlayerSpecialKeysPressed.size === 3) {
              secondFighter.health -= 2 * firstFighter.attack;
              lastTimeOfHitFirstPlayer = Date.now();
              // После нанесения урона, очищам множество, чтоб не было "залипания" клавиш
              firstPlayerSpecialKeysPressed.clear();
            }
          } 

          if (lastTimeOfHitSecondPlayer + SECOND * 10 < Date.now()) {
            if (controls.PlayerTwoCriticalHitCombination.includes(event.code)) {
              secondPlayerSpecialKeysPressed.add(event.code);
            };

            if(secondPlayerSpecialKeysPressed.size === 3) {
              firstFighter.health -= 2 * secondFighter.attack;
              lastTimeOfHitSecondPlayer = Date.now();
              secondPlayerSpecialKeysPressed.clear();
            }
          }
        }
        updateHealthIndicators();

        if(firstFighter.health <= 0){
          removeEventListenersFromDocument()
          resolve(secondFighter);
        } else if (secondFighter.health <= 0) {
          removeEventListenersFromDocument();
          resolve(firstFighter);
        }
    }
    
    function handleKeyup(event){
      switch (event.code) {
        case controls.PlayerOneBlock:
          firstFighter.defense = fighterOneStarterDefense;
          break;
        
        case controls.PlayerTwoBlock:
          secondFighter.defense = fighterTwoStarterDefense;
          break;

        // Если игрок отпустил не клавишу блока, тогда скорее всего он отпустил клавишу специальных приемов
        // и мы удаляем ее из множества нажатых, даже если это была не клавиша специальных приемов и ее нет в множестве,
        // попытка удаления ошибки не вызовет
        default:
          setTimeout(()=>{
            firstPlayerSpecialKeysPressed.delete(event.code);
            secondPlayerSpecialKeysPressed.delete(event.code);
          }, 100)
          
      }
    }

    function removeEventListenersFromDocument(){
      document.removeEventListener('keydown', handleKeydown);
      document.removeEventListener('keyup', handleKeyup);
    }

    function updateHealthIndicators(){
      firstHealthIndicator.style.width = calculateIndicatorHealthWidth(fighterOneStarterHealth, firstFighter.health) + "%";
      secondHealthIndicator.style.width = calculateIndicatorHealthWidth(fighterTwoStarterHealth, secondFighter.health) + "%";
    }
  });
  
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);

  const damage = hitPower - blockPower;
  
  return damage > 0 ? damage: 0;
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = 1 + Math.random();

  const power = attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = 1 + Math.random();

  const power = defense * dodgeChance;

  return power; 
}

function calculateIndicatorHealthWidth(starterHealth, currentHealth) {
 const width = 100 * currentHealth / starterHealth;
 return width < 0 ? 0: width;
}